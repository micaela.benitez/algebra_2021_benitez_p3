﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomMath;

namespace RoomTesterScript
{
    public class RoomTester : MonoBehaviour
    {
        const int totalEdges = 8;

        PlaneAux downPlane;
        PlaneAux topPlane;
        PlaneAux frontPlane;
        PlaneAux backPlane;
        PlaneAux leftPlane;
        PlaneAux rightPlane;

        public Vector3 pointAux;
        public Vec3 point;

        public Transform cube;
        private MeshRenderer cubeMesh;
        private List<Vec3> cubeEdges = new List<Vec3>();
        private bool inside = true;

        private void Awake()
        {
            cubeMesh = cube.gameObject.GetComponent<MeshRenderer>();

            for (int i = 0; i < totalEdges; i++)
            {
                cubeEdges.Add(Vec3.Zero);
            }
        }

        private void Start()
        {
            Vec3 point1 = new Vec3(5.0f, 0.0f, 0.0f);
            Vec3 point2 = new Vec3(0.0f, 0.0f, 5.0f);
            Vec3 point3 = new Vec3(-5.0f, 0.0f, 0.0f);
            Vec3 point4 = new Vec3(0.0f, 0.0f, -5.0f);
            Vec3 point5 = new Vec3(0.0f, 5.0f, 0.0f);
            Vec3 point6 = new Vec3(0.0f, -5.0f, 0.0f);

            // Creo el cuarto (las 6 paredes)
            downPlane = new PlaneAux(point6.normalized, point6);
            topPlane = new PlaneAux(point5.normalized, point5);
            frontPlane = new PlaneAux(point4.normalized, point4);
            leftPlane = new PlaneAux(point3.normalized, point3);
            backPlane = new PlaneAux(point2.normalized, point2);
            rightPlane = new PlaneAux(point1.normalized, point1);

            downPlane.Flip();
            topPlane.Flip();
            frontPlane.Flip();
            leftPlane.Flip();
            backPlane.Flip();
            rightPlane.Flip();
        }

        private void Update()
        {
            point = new Vec3(pointAux);
            PointInsideRoom();   // Chequeo si el punto esta dentro de las 6 paredes o no

            EdgesCalculation();   // Calculo los vertices del cubo
            CubeInsideRoom();   // Chequeo si el cubo esta dentro de las 6 paredes o no
        }

        public void PointInsideRoom()
        {
            if (downPlane.GetSide(point) && topPlane.GetSide(point) && frontPlane.GetSide(point) &&
                backPlane.GetSide(point) && leftPlane.GetSide(point) && rightPlane.GetSide(point))
                Debug.Log("El punto esta dentro");
            else
                Debug.Log("El punto NO esta dentro");
        }

        public void EdgesCalculation()
        {
            Vec3 pos = new Vec3(cube.position);
            Vec3 scale = new Vec3(cube.lossyScale / 2);
            Vec3 up = new Vec3(cube.up);
            Vec3 forward = new Vec3(cube.forward);
            Vec3 right = new Vec3(cube.right);

            for (int i = 0; i < cubeEdges.Count; i++)
            {
                cubeEdges[i] = pos + (up * scale.y);

                if (i == 0 || i == 1 || i == 4 || i == 5) cubeEdges[i] += (forward * scale.z);
                else cubeEdges[i] -= (forward * scale.z);

                if (i % 2 == 0) cubeEdges[i] += (right * scale.x);
                else cubeEdges[i] -= (right * scale.x);
            }
        }

        public void CubeInsideRoom()
        {
            inside = false;
            for (int i = 0; i < cubeEdges.Count; i++)
            {
                if (downPlane.GetSide(cubeEdges[i]) && topPlane.GetSide(cubeEdges[i]) &&
                  frontPlane.GetSide(cubeEdges[i]) && backPlane.GetSide(cubeEdges[i]) &&
                  leftPlane.GetSide(cubeEdges[i]) && rightPlane.GetSide(cubeEdges[i]))
                    inside = true;
            }

            if (inside)
            {
                Debug.Log("El cubo esta dentro");
                cubeMesh.enabled = true;
            }
            else
            {
                Debug.Log("El cubo NO esta dentro");
                cubeMesh.enabled = false;
            }
        }
    }
}